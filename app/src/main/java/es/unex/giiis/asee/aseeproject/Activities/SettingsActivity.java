package es.unex.giiis.asee.aseeproject.Activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import es.unex.giiis.asee.aseeproject.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ActionBar ab = getSupportActionBar();

        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(R.string.settings);
        ab.setDisplayHomeAsUpEnabled(true);
        String barColor = PreferenceManager.getDefaultSharedPreferences(this).getString("color_tema", String.valueOf(getResources().getColor(R.color.green)));
        ab.setBackgroundDrawable(new ColorDrawable( (Color.parseColor(barColor)) ));
    }
}

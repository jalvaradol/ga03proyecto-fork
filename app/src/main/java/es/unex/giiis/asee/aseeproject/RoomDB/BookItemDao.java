package es.unex.giiis.asee.aseeproject.RoomDB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.asee.aseeproject.Models.BookItem;

@Dao
public interface BookItemDao {

    @Query("SELECT * FROM book")
    public LiveData<List<BookItem>> getAll();

    @Query("SELECT * FROM book WHERE isbn =:isbn")
    public LiveData<List<BookItem>> getByIsbn(String isbn);

    @Query("SELECT * FROM book WHERE category=:categoryName")
    public LiveData<List<BookItem>> getByCategory(String categoryName);

    @Insert
    public long insert(BookItem item);

    @Query("DELETE FROM book")
    public void deleteAll();

    @Query("SELECT * FROM book LIMIT 1")
    public List<BookItem> existData();

}

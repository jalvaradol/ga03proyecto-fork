package es.unex.giiis.asee.aseeproject.Repository;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import es.unex.giiis.asee.aseeproject.AppExecutors;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.Models.Bookshelf;
import es.unex.giiis.asee.aseeproject.Models.Item;
import es.unex.giiis.asee.aseeproject.Networking.LoadBooks;
import es.unex.giiis.asee.aseeproject.Networking.RetrofitManager;
import es.unex.giiis.asee.aseeproject.RoomDB.BookDatabase;
import es.unex.giiis.asee.aseeproject.RoomDB.BookItemDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookRepository {

    private Context context;
    private static BookRepository instance;
    private static RetrofitManager retrofit;
    private final AppExecutors mExecutors;
    private final BookItemDao mbookItemDao;
    private MutableLiveData<List<BookItem>> networkData;
    private static List<BookItem> bookitems;
    LiveData<List<BookItem>> data;

    public BookRepository (Context context){
        this.context = context;
        mbookItemDao = BookDatabase.getDatabase(context).bookItemDao();
        mExecutors = AppExecutors.getInstance();
        networkData = new MutableLiveData<>();
    }

    public static BookRepository getInstance(Context context) {
        if (instance == null) {
            return new BookRepository(context);
        }
        else
            return instance;
    }

    public LiveData<List<BookItem>> loadBooks (){
        data= new LoadBooks().loadBooks(context, mbookItemDao,mExecutors,networkData);

        return data;
    }


    public LiveData<List<BookItem>> getAll () {

        LiveData<List<BookItem>> data = mbookItemDao.getAll();

        final Boolean vacia;

        vacia = mbookItemDao.existData().isEmpty();

        Executor executor= new Executor() {
            @Override
            public void execute(@NonNull Runnable command) {
                new Thread(command).start();
            }
        };

        executor.execute(new Runnable() {
            @Override
            public void run() {
                if(vacia){
                    Log.i("tag", "BASE DE DATOS VACIA");
                    loadBooks();
                }
            }
        });

        return data;

    }

    public LiveData<List<BookItem>> getByIsbn (String isbn) {

        LiveData<List<BookItem>> data = mbookItemDao.getByIsbn(isbn);


        Executor executor= new Executor() {
            @Override
            public void execute(@NonNull Runnable command) {
                new Thread(command).start();
            }
        };

        return data;
    }

    public LiveData<List<BookItem>> getByCategory (String categoryName) {

        LiveData<List<BookItem>> data = mbookItemDao.getByCategory(categoryName);

        final Boolean vacia;

        vacia = mbookItemDao.existData().isEmpty();

        Executor executor= new Executor() {
            @Override
            public void execute(@NonNull Runnable command) {
                new Thread(command).start();
            }
        };

        executor.execute(new Runnable() {
            @Override
            public void run() {
                if(vacia){
                    Log.i("tag", "BASE DE DATOS VACIA");
                    loadBooks();
                }
            }
        });

        return data;
    }
}

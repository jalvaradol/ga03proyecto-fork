package es.unex.giiis.asee.aseeproject.Fragments;


import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.picasso.Picasso;

import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.R;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.ViewModel.DetailsViewModel;
import es.unex.giiis.asee.aseeproject.ViewModel.DetailsViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookDetailsFragment extends Fragment {
    //Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    //Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public static final String BOOK_DATA = "bookData";

    private ImageView backdropImageView;
    private ImageView posterImageView;
    private TextView titleTextView;
    private TextView overviewTextView;
    private TextView releaseDateTextView;
    private TextView authorTextView;
    private TextView isbnTextView;
    private TextView ratingTextView;
    private ImageView favouriteImageView;

    private DetailsViewModel mDetailsViewModel;
    private BookItem book;
    private int numberOfFavourites = 0;

    public BookDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookDetailsFragment.
     */
    //Rename and change types and number of parameters
    public static BookDetailsFragment newInstance(String param1, String param2) {
        BookDetailsFragment fragment = new BookDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_book_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        book = (BookItem) bundle.getSerializable("bookItem");

        DetailsViewModelFactory factory = DetailsViewModelFactory.getInstance(getContext());
        mDetailsViewModel = ViewModelProviders.of(this, factory).get(DetailsViewModel.class);

        //Check if the book data has been sent through the activity intent properly
        /*if((book = getIntent().getParcelableExtra(BOOK_DATA)) == null)
            book = new Book();
        else
            book = getIntent().getParcelableExtra(BOOK_DATA);*/

        //Get the poster ImageView and set the corresponding picture from the internal storage or an api query
        posterImageView = (ImageView) getView().findViewById(R.id.iv_book_details_poster);
        Picasso.get().load(book.getImage()).resize(190, 300).into(posterImageView);


        //Get the backdrop ImageView and set the corresponding picture from the internal storage or an api query
        backdropImageView = (ImageView) getView().findViewById(R.id.iv_book_details_backdrop);
        Picasso.get().load(book.getImage()).resize(190, 300).centerCrop().into(backdropImageView);
        //Get the screen dimension to determinate, with the picture aspect ratio (1.785) the height that the imageView will have
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        ViewGroup.LayoutParams bdlp = backdropImageView.getLayoutParams();
        bdlp.width = display.getWidth();
        bdlp.height = (int) (bdlp.width / 1.785);
        backdropImageView.setLayoutParams(bdlp);
        backdropImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);



        //set the title textView text
        titleTextView = (TextView) getView().findViewById(R.id.tv_book_details_title);
        titleTextView.setText(book.getTitle());

        //set the overview textView text
        overviewTextView = (TextView) getView().findViewById(R.id.tv_book_details_overview);
        overviewTextView.setText(book.getSynopsis());

        //set the releaseDate textView text, parsing the date with the correspondant format
        releaseDateTextView = (TextView) getView().findViewById(R.id.tv_book_details_release_date);
        releaseDateTextView.setText(new SimpleDateFormat("yyyy", Locale.US).format(book.getPublishdate()));

        //set the author textView text
        authorTextView = (TextView) getView().findViewById(R.id.tv_book_details_author);
        authorTextView.setText(book.getAuthor());

        //set the isbn textView text
        isbnTextView = (TextView) getView().findViewById(R.id.tv_book_details_isbn);
        isbnTextView.setText(book.getIsbn());

        //set the rating textView text
        ratingTextView = (TextView) getView().findViewById(R.id.tv_book_details_pages);
        ratingTextView.setText(book.getNumpages() + " " + this.getResources().getString(R.string.book_details_pages));

        //set the favourite imageView fill or empty depending of the isFavourite boolean value
        favouriteImageView = (ImageView) getView().findViewById(R.id.iv_book_details_favourite);
        updateFavouriteIcon(book);
        favouriteImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addRemoveFavourite(book);
            }
        });

    }

    private BookFavouriteItem bookItemToBookFavouriteItem(BookItem book){
        BookFavouriteItem bookFavouriteItem = new BookFavouriteItem(book.getId(), book.getTitle(), book.getIsbn(), book.getAuthor(), book.getPublishdate(), book.getSynopsis(), book.getCategory(), book.getImage(), book.getNumpages());

        return bookFavouriteItem;
    }

    //Method that checks if the book is favourite or not and updates its value in the database with the opposite value
    private void addRemoveFavourite(BookItem book) {
        //BookFavouriteRepository bookFavouriteRepository = BookFavouriteRepository.getInstance(getContext());
        mDetailsViewModel.getByIsbn(book.getIsbn()).observe(this, bookfavouriteitems -> {
            numberOfFavourites = bookfavouriteitems.size();
        });

        if(numberOfFavourites == 0) {
            mDetailsViewModel.insert(bookItemToBookFavouriteItem(book));
            numberOfFavourites++;
            Toast.makeText(getActivity(), R.string.book_details_addedFavourite, Toast.LENGTH_LONG).show();
        }
        else {
            mDetailsViewModel.deleteByIsbn(book.getIsbn());
            numberOfFavourites--;
            Toast.makeText(getActivity(), R.string.book_details_removedFavourite, Toast.LENGTH_LONG).show();
        }


        //Updates the favourite imageView to match the isFavourite value
        if(numberOfFavourites > 0) {
            favouriteImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_recommend_fill_black));
        } else {
            favouriteImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_recommend_empty_black));
        }
    }


    //Updates the favourite imageView to match the isFavourite value
    private void updateFavouriteIcon(BookItem book) {
        //BookFavouriteRepository bookFavouriteRepository = BookFavouriteRepository.getInstance(getContext());

        mDetailsViewModel.getByIsbn(book.getIsbn()).observe(this, bookfavouriteitems -> {
            numberOfFavourites = bookfavouriteitems.size();

            if(numberOfFavourites > 0) {
                favouriteImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_recommend_fill_black));
            } else {
                favouriteImageView.setImageDrawable(getActivity().getDrawable(R.drawable.ic_recommend_empty_black));
            }
        });


    }
}

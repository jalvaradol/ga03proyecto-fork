package es.unex.giiis.asee.aseeproject.Networking;

import android.content.Context;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.aseeproject.AppExecutors;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.Models.Bookshelf;
import es.unex.giiis.asee.aseeproject.Models.Item;
import es.unex.giiis.asee.aseeproject.RoomDB.BookDatabase;
import es.unex.giiis.asee.aseeproject.RoomDB.BookItemDao;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitManager {

    private String BASE_URI = "https://www.googleapis.com/books/v1/";
    private Context context;
    private static Retrofit retrofit = null;


    public RetrofitManager (Context context){
        this.context = context;
    }

    public ApiService getApiService() {
        // Create a very simple REST adapter which points the API.
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URI)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our API interface.
        return retrofit.create(ApiService.class);
    }
}

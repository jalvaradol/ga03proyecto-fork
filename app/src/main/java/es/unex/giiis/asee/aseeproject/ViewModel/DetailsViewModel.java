package es.unex.giiis.asee.aseeproject.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.Repository.BookFavouriteRepository;

public class DetailsViewModel extends ViewModel {

    private final BookFavouriteRepository mBookFavouriteRepository;
    private  LiveData<List<BookItem>> mFavouriteBookByIsbn;


    public DetailsViewModel(BookFavouriteRepository bookFavouriteRepository) {
        this.mBookFavouriteRepository = bookFavouriteRepository;
    }

    public void insert (BookFavouriteItem bookFavouriteItem) {
        mBookFavouriteRepository.insert(bookFavouriteItem);
    }


    public LiveData<List<BookItem>> getByIsbn(String isbn) {
        mFavouriteBookByIsbn = mBookFavouriteRepository.getByIsbn(isbn);
        return mFavouriteBookByIsbn;
    }

    public void deleteByIsbn (String isbn) {
        mBookFavouriteRepository.deleteByIsbn(isbn);
    }
}

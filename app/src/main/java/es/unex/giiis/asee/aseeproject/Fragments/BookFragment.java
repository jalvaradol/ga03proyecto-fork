package es.unex.giiis.asee.aseeproject.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import es.unex.giiis.asee.aseeproject.Activities.BookDetailsActivity;
import es.unex.giiis.asee.aseeproject.Activities.MainActivity;
import es.unex.giiis.asee.aseeproject.Adapters.MyAdapter;
import es.unex.giiis.asee.aseeproject.R;
import es.unex.giiis.asee.aseeproject.Models.BookItem;
import es.unex.giiis.asee.aseeproject.ViewModel.MainViewModel;
import es.unex.giiis.asee.aseeproject.ViewModel.MainViewModelFactory;

/*
    Steps:
    * Create empty project
    * Declare INTERNET permission
    * Add dependencies:
        - com.squareup.retrofit2
        - com.squareup.retrofit2:converter-gson
    * Add pojos: Bookshelf from jsonschema2pojo
    * Define service interface
    * Create REST adapter
    * Create a service implementation
    * Create a call
    * Use the call async'ly
        - define callbacks: onReponse & onFailure
 */

/**
 * A fragment representing a list of Items.
 * <p/>
 */
public class BookFragment extends Fragment implements MyAdapter.OnListInteractionListener {

    //Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    //Customize parameters
    private int mColumnCount = 3;
    private OnListFragmentInteractionListener mListener;
    private MyAdapter mAdapter;
    private MainViewModel mViewModel;




    @Override
    public void onListInteraction(BookItem bookItem) {
        Intent intent = new Intent(getContext(), BookDetailsActivity.class);
        intent.putExtra("bookItem", bookItem);
        startActivity(intent);
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BookFragment() {
    }

    //Customize parameter initialization
    @SuppressWarnings("unused")
    public static BookFragment newInstance(int columnCount) {
        BookFragment fragment = new BookFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            recyclerView.setLayoutManager(new GridLayoutManager(context, 3));

            List<BookItem> myDataset = new ArrayList<BookItem>();
            mAdapter = new MyAdapter(myDataset, this, context);
            recyclerView.setAdapter(mAdapter);

            MainViewModelFactory factory = MainViewModelFactory.getInstance(getContext());
            mViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);

            mViewModel.getAll();

            if (((MainActivity) getActivity()).getCurrentFragment() == 3) {

                mViewModel.getAllFavourites().observe(this, bookItems -> {
                    mAdapter.swap(bookItems);
                });

                ((MainActivity) getActivity()).setCurrentFragment(0);
            }
            else if (((MainActivity) getActivity()).getCurrentFragment() == 2){
                Bundle bundle = getArguments();
                String categoryName = bundle.getSerializable("categoryName").toString();

                mViewModel.getByCategory(categoryName).observe(this, bookItems -> {
                    mAdapter.swap(bookItems);
                });

                ((MainActivity) getActivity()).setCurrentFragment(0);
            } else {

                mViewModel.getAll().observe(this, bookItems -> {
                    mAdapter.swap(bookItems);
                });

            }


        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        //Update argument type and name
        void onListFragmentInteraction(BookItem item);
    }
}

package es.unex.giiis.asee.aseeproject.Models;

import android.content.Intent;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import es.unex.giiis.asee.aseeproject.RoomDB.DateConverter;

@Entity(tableName = "favouriteBooks")
public class BookFavouriteItem implements Serializable {

    @Ignore
    public final static String ID = "ID";

    @Ignore
    public final static String TITLE = "title";

    @Ignore
    public final static String AUTHOR = "author";

    @Ignore
    public final static String ISBN = "isbn";

    @Ignore
    public final static String PUBLISHDATE = "publishdate";

    @Ignore
    public final static String SYNOPSIS = "synopsis";

    @Ignore
    public final static String CATEGORY = "category";

    @Ignore
    public final static String IMAGE = "image";

    @Ignore
    public final static String NUMPAGES = "numpages";

    @Ignore
    public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
            "yyyy", Locale.US);

    @PrimaryKey
    private long id;
    private String title;
    private String author;
    private String isbn;

    @TypeConverters(DateConverter.class)
    private Date publishdate = new Date();

    private String synopsis;
    private String category;
    private String image;

    private String numpages;

    @Ignore
    public BookFavouriteItem (){

    }

    public BookFavouriteItem(long id, String title, String isbn, String author, Date publishdate, String synopsis, String category, String image, String numpages) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.publishdate = publishdate;
        this.synopsis = synopsis;
        this.category = category;
        this.image = image;
        this.numpages = numpages;
    }
    @Ignore
    public BookFavouriteItem(long id, String title, String isbn, String author, String publishDate, String synopsis, String category, String image, String numpages) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.author = author;
        try {
            publishdate = BookFavouriteItem.FORMAT.parse(publishDate);
        } catch(ParseException e){
            publishdate = new Date();
        }
        this.synopsis = synopsis;
        this.category = category;
        this.image = image;
        this.numpages = numpages;
    }

    @Ignore
    BookFavouriteItem(Intent intent){
        id = intent.getLongExtra(BookFavouriteItem.ID, 0);
        title = intent.getStringExtra(BookFavouriteItem.TITLE);
        author = intent.getStringExtra(BookFavouriteItem.AUTHOR);
        isbn = intent.getStringExtra(BookFavouriteItem.ISBN);

        try{
            publishdate = BookFavouriteItem.FORMAT.parse(intent.getStringExtra(BookFavouriteItem.PUBLISHDATE));
        }
        catch (ParseException e){
            publishdate = new Date();
        }

        synopsis = intent.getStringExtra(BookFavouriteItem.SYNOPSIS);
        category = intent.getStringExtra(BookFavouriteItem.CATEGORY);
        image = intent.getStringExtra(BookFavouriteItem.IMAGE);
        numpages = intent.getStringExtra(BookFavouriteItem.NUMPAGES);

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Date getPublishdate() {
        return publishdate;
    }

    public void setPublishdate(Date publishdate) {
        this.publishdate = publishdate;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNumpages() {
        return numpages;
    }

    public void setNumpages(String numpages) {
        this.numpages = numpages;
    }

    public static void packageIntent (Intent intent, String bTitle, String bAuthor, String bIsbn, String publishDate, String bSynopsis, String bCategory, String bImage, String bNumPages){
        intent.putExtra(BookFavouriteItem.TITLE, bTitle);
        intent.putExtra(BookFavouriteItem.AUTHOR, bAuthor);
        intent.putExtra(BookFavouriteItem.ISBN, bIsbn);
        intent.putExtra(BookFavouriteItem.PUBLISHDATE, publishDate);
        intent.putExtra(BookFavouriteItem.SYNOPSIS, bSynopsis);
        intent.putExtra(BookFavouriteItem.CATEGORY, bCategory);
        intent.putExtra(BookFavouriteItem.IMAGE, bImage);
        intent.putExtra(BookFavouriteItem.NUMPAGES, bNumPages);
    }

    @Override
    public String toString() {
        return "BookFavouriteItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", isbn='" + isbn + '\'' +
                ", publishdate=" + publishdate +
                ", synopsis='" + synopsis + '\'' +
                ", category='" + category + '\'' +
                ", image='" + image + '\'' +
                ", numpages='" + numpages + '\'' +
                '}';
    }
}

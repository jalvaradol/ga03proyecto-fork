package es.unex.giiis.asee.aseeproject.RoomDB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.asee.aseeproject.Models.BookFavouriteItem;
import es.unex.giiis.asee.aseeproject.Models.BookItem;

@Dao
public interface BookFavouriteItemDao {

    @Query("SELECT * FROM favouriteBooks")
    public LiveData<List<BookItem>> getAll();

    @Query("SELECT * FROM favouriteBooks WHERE isbn =:isbn")
    public LiveData<List<BookItem>> getByIsbn(String isbn);

    @Query("DELETE FROM favouriteBooks WHERE isbn = :isbn")
    public void deleteByIsbn(String isbn);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insert(BookFavouriteItem item);

    @Query("DELETE FROM favouriteBooks")
    public void deleteAll();



}

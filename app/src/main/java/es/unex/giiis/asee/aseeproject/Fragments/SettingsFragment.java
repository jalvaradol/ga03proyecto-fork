package es.unex.giiis.asee.aseeproject.Fragments;

import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import es.unex.giiis.asee.aseeproject.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);

    }
}

package es.unex.giiis.asee.aseeproject.RoomDB;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Room;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.aseeproject.Models.BookItem;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

public class BookItemDaoUnitTest {
    private BookItemDao bookItemDao;
    private BookDatabase db;

    // necessary to test the LiveData
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();


    @Before
    public void createDb() throws Exception {
        //create a mock database, using our RoomDatabase implementation (AppDatabase.class)
        Context context = getInstrumentation().getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, BookDatabase.class).allowMainThreadQueries().fallbackToDestructiveMigration().build();
        //take the dao from the created database
        bookItemDao = db.bookItemDao();
    }


    @After
    public void closeDb() throws Exception {
        //closing database
        db.close();
    }

    @Test
    public void getAll() throws ParseException, InterruptedException {
        //creamos dos libros
        BookItem b1 = createBook(998);
        BookItem b2 = createBook(999);

        //insercion en la bd
        bookItemDao.insert(b1);
        bookItemDao.insert(b2);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookItemDao.getAll();
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);
        //check that one of the elements is the first center
        assertTrue(books.get(0).getId() == 998 || books.get(0).getId() == 999);
        //check that one of the elements is the second center
        assertTrue(books.get(1).getId() == 998 || books.get(1).getId() == 999);
        //check that the same center does not return us twice
        assertFalse(books.get(0).getId() == books.get(1).getId());


    }

    //Test correspondiente al UC02
    @Test
    public void getByIsbn() throws ParseException, InterruptedException {
        //creamos dos libros
        BookItem b1 = createBook(999);

        //insercion en la bd
        bookItemDao.insert(b1);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookItemDao.getByIsbn("isbn");
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);
        //check that one of the elements is the first center
        assertTrue(books.get(0).getIsbn().equals("isbn"));
    }

    //Test correspondiente al UC11
    @Test
    public void getByCategory() throws ParseException, InterruptedException {
        //creamos dos libros
        BookItem b1 = createBook(999);

        //insercion en la bd
        bookItemDao.insert(b1);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookItemDao.getByCategory("category");
        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);
        //check that one of the elements is the first center
        assertTrue(books.get(0).getCategory().equals("category"));
    }

    @Test
    public void insert() throws ParseException, InterruptedException {
        //creamos dos libros
        BookItem b1 = createBook(998);
        BookItem b2 = createBook(999);

        //insercion en la bd
        bookItemDao.insert(b1);
        bookItemDao.insert(b2);

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookItemDao.getAll();

        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);

        assertFalse(books.size() != 2);

    }

    @Test
    public void deleteAll() throws ParseException, InterruptedException {
        //creamos dos libros
        BookItem b1 = createBook(998);
        BookItem b2 = createBook(999);

        //insercion en la bd
        bookItemDao.insert(b1);
        bookItemDao.insert(b2);

        //borrado en la bd
        bookItemDao.deleteAll();

        //we recover the books (in a LiveData)
        LiveData<List<BookItem>> livebooks = bookItemDao.getAll();

        //we retrieve LiveData information with getValue() from LiveDataTestUtils
        List<BookItem> books = LiveDataTestUtils.getValue(livebooks);


        assertTrue(books.size() == 0);
    }

    @Test
    public void existData() throws ParseException, InterruptedException {

        //we recover the books (in a LiveData)
        List<BookItem> livebooks = bookItemDao.existData();

        assertTrue(livebooks.size() == 0);

    }

    //auxiliar method to create more easily centers to the tests
    public static BookItem createBook(Integer id) throws ParseException {
        BookItem b = new BookItem();
        b.setId(id);
        b.setAuthor("author");
        b.setTitle("title");
        b.setCategory("category");
        b.setImage("image");
        b.setIsbn("isbn");
        b.setPublishdate(new SimpleDateFormat("yyyy", Locale.US).parse("1900"));
        b.setNumpages("10");
        b.setSynopsis("synopsis");

        return b;
    }
}